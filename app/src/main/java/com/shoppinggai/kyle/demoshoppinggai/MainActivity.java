package com.shoppinggai.kyle.demoshoppinggai;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //Facebook
    private AccessToken accessToken;
    private Profile profile;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private boolean isSetPreference = false;
    ProfilePictureView profilePictureView;
    TextView tvFbName, tvFbEmail;

    //ShoppingGaiAPI
    private String apiToken;

    //Widgets
    LoginButton loginButton;
    NavigationView navigationView;

    //Variable
    String fbID, fbName, fbEmail;
    View header;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor mEditor;
    String spData = "FB_DATA";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());


        AppEventsLogger.activateApp(getApplication());
        setContentView(R.layout.activity_main);

        //Facebook
        callbackManager = CallbackManager.Factory.create();


        //Register widgets
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        header = navigationView.getHeaderView(0);
        loginButton = (LoginButton) header.findViewById(R.id.login_button);
        tvFbName = (TextView) header.findViewById(R.id.fbName);
        tvFbEmail = (TextView) header.findViewById(R.id.fbEmail);
        profilePictureView = (ProfilePictureView) header.findViewById(R.id.profilepic);
        profilePictureView.setCropped(true);
        profilePictureView.setDefaultProfilePicture(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));


        //Check login status
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                updateWithToken(currentAccessToken);
            }
        };

        accessToken = AccessToken.getCurrentAccessToken();
        updateWithToken(accessToken);

        //Facebook Login Handling
        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                profile = Profile.getCurrentProfile();
                Log.d("Facebook", "Access token has been stored");
                Log.d("Facebook", "ProfileStatus: " + profile.toString());


                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("Facebook", response.toString());
                        fbID = object.optString("id");
                        fbName = object.optString("name");
                        fbEmail = object.optString("email");

                        sharedPreferences = getSharedPreferences(spData, 0);
                        mEditor = sharedPreferences.edit();
                        mEditor.putString("fbID", fbID);
                        mEditor.putString("fbName", fbName);
                        mEditor.putString("fbEmail", fbEmail);
                        mEditor.apply();

                        mEditor.clear();

                        isSetPreference = true;

                        profilePictureView.setProfileId(fbID);
                        tvFbName.setText(profile.getName());
                        tvFbEmail.setText(fbEmail);
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,friends");
                request.setParameters(parameters);
                request.executeAsync();

                try {
                    apiToken = new RequestType("GET_API_TOKEN").execute("http://192.168.1.120:8000/oauth/token", "password", "1",
                            "86f4GBun596P9bbuALugSeI10vMevd7G0leJxv3l", "kaluveliza@hotmail.com", "5612520", "").get();

                    Log.d("New API TOKEN", apiToken);

                    String mdfApiToken = apiToken.replace("null", "");

                    JSONObject jsonObject = new JSONObject(mdfApiToken);
                    apiToken = jsonObject.getString("access_token");

                    sharedPreferences = getSharedPreferences(spData, 0);
                    mEditor = sharedPreferences.edit();
                    mEditor.putString("API_TOKEN", apiToken).apply();
                    mEditor.clear();


                    Log.d("Translated Api Token", apiToken);

                } catch (Exception e) {
                    Log.e("Error", e.toString());
                }
            }

            @Override
            public void onCancel() {
                Log.d("Facebook", "User canceled login action");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("Facebook", "Error occur:" + error.toString());
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_information) {

            Profile_Client profileClient = new Profile_Client();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.Content_layout,profileClient,profileClient.getTag()).commit();

        } else if (id == R.id.nav_product) {

            ProductList productList = new ProductList();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.Content_layout,productList,productList.getTag()).commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Pass the result to the CallbackManager
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //Check Facebook user is logged
    private void updateWithToken(AccessToken currentAccessToken) {

        if (currentAccessToken != null) {
            Log.d("updateWithToken", "have token");
            sharedPreferences = getSharedPreferences(spData, 0);
            fbID = sharedPreferences.getString("fbID", "Wait ... ");
            fbName = sharedPreferences.getString("fbName", "Wait ... ");
            fbEmail = sharedPreferences.getString("fbEmail", "Wait ... ");
            apiToken = sharedPreferences.getString("API_TOKEN",null);

            if (!fbID.equals("")) {
                GraphRequest request = GraphRequest.newMeRequest(currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        profilePictureView.setProfileId(object.optString("id"));
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "picture,name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            tvFbName.setText(fbName);
            tvFbEmail.setText(fbEmail);

        } else {
            Log.d("updateWithToken", "no token bye bye");
            if (isSetPreference) {
                mEditor.remove("fbID");
                mEditor.remove("fbName");
                mEditor.remove("fbEmail");
                mEditor.remove("API_TOKEN");
                mEditor.apply();
                isSetPreference = false;
            }

            GraphRequest request = GraphRequest.newMeRequest(currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    profilePictureView.setProfileId(null);
                }
            });

            request.executeAsync();

            tvFbName.setText(R.string.nav_header_fbName);
            tvFbEmail.setText(R.string.nav_header_fbEmail);
        }
    }
}





