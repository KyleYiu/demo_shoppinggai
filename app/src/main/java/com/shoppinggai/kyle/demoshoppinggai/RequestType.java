package com.shoppinggai.kyle.demoshoppinggai;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Kyle on 23/9/2016.
 */
public class RequestType extends AsyncTask<String, String, String> {

    HttpURLConnection conn;
    URL url;
    InputStream is = null;
    OutputStream os = null;
    BufferedReader br = null;
    String charset = "UTF-8";
    String requestType, query, line, result = null;



    public RequestType(String requestType) {
        this.requestType = requestType;
    }

    @Override
    protected String doInBackground(String... params) {

        try {

            if (requestType.equals("GET_API_TOKEN")) {
                url = new URL(params[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Cache-Control", "no-cache");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                query = "grant_type=" + params[1] + "&client_id=" + params[2] +
                        "&client_secret=" + params[3] + "&username=" + params[4] +
                        "&password=" + params[5] + "&scope=" + params[6];

                os = conn.getOutputStream();
                os.write(query.getBytes(charset));
                os.flush();
                os.close();
            }

            is = conn.getInputStream();

            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                result += line + "\n";
            }

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

/*        if (requestType.equals("GET_API_TOKEN")) {
            String apiResult[] = new String[2];
            try {
                JSONObject jsonObject = new JSONObject(result);
                apiResult[0] =  jsonObject.getString("token_type");
                apiResult[1] = jsonObject.getString("expires_in");
                apiResult[2] = jsonObject.getString("access_token");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }*/

    }

}
